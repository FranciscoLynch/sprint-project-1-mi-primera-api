const chalk = require('chalk');
const e = require('express');
const { Op } = require('sequelize');
const db = require('../database/sync');

const jwt = require('jsonwebtoken');
const { key } = require('../config');

// REGISTRO DE USUARIO 

// VALIDO QUE LOS CAMPOS NO ESTEN VACIOS PARA EL REGISTRO DE USUARIO
const valFieldSgnUp = async (req, res, next) => {
    const { user, password, name, lastname, email, phone } = req.body;

    if (user === "" || password === "" || name === "" || lastname === "" || email === "" || phone === "") {
        console.log(chalk.red('Por favor, verifique que esta ingresando los datos correctamente'));
        res.status(400).send('Por favor, verifique que esta ingresando los datos correctamente');
    } else {
        next();
    }
};

// VALIDO QUE NO SE REPITA EL EMAIL 
const valEmail = async (req, res, next) => {
    const { email } = req.body

    const user = await db.User.findOne({
        where: {
            email: {
                [Op.eq]: email
            }
        }
    });

    if (user != null) {
        console.log(chalk.red('El email ya ha sido utilizado'));
        res.status(406).send(`El email ${user.email} ya ha sido utilizado`);
    } else {
        return next();
    }
};

// --------------------------------------------------------------------------------------------------------------------------------------------------- \\

// LOGIN 

// VALIDO QUE AL INICIAR SESION EL USUARIO NO ESTE SUSPENDIDO 
const valSuspension = async (req, res, next) => {
    const { email } = req.body;
    try {
        const user = await db.User.findOne({
            where: {
                email: {
                    [Op.eq]: email
                },
                active: {
                    [Op.eq]: 0
                }
            }
        });
        if (user != null) {
            console.log(chalk.red('Su cuenta fue suspendida, no puede iniciar sesion'));
            res.status(202).send('Su cuenta fue suspendida, no puede iniciar sesion');
        } else {
            next();
        }
    } catch (error) {
        console.log(chalk.red('Ha ocurrido un error - LINEA 67 ', error));
        res.status(404).json('Ha ocurrido un error ', error);
    }
}


// VALIDO QUE LOS CAMPOS NO ESTEN VACIOS PARA EL INICIO Y CIERRE DE SESION
const valFieldLogin = async (req, res, next) => {
    const { email, password } = req.body;

    if (email === "" || password === "") {
        console.log(chalk.red('Por favor, verifique que esta ingresando los datos correctamente'));
        res.status(400).send('Por favor, verifique que esta ingresando los datos correctamente');
    } else {
        next();
    }
};

// VALIDO SI YA INICIO SESION ( ESTO ES POR SI DESPUES DE HABER INICIADO SESION VUELVE A INTENTARLO )
const valLogin = async (req, res, next) => {
    const { email } = req.body;

    const user = await db.User.findOne({
        where: {
            email: {
                [Op.eq]: email
            },
            login: {
                [Op.eq]: true
            }
        }
    });
    console.log({ user });
    if (user != null) {
        console.log(chalk.red('La sesion del usuario ya esta iniciada'));
        res.status(406).send('La sesion del usuario ya esta iniciada');
    } else {
        next();
    }

}

// VALIDO QUE ESTE LOGUEADO 
const valLogged = async (req, res, next) => {
    const token = req.headers.authorization.replace('Bearer ', '');

    const data = jwt.decode(token, key);
    console.log(chalk.red('ESTA ES LA DATA >>', data)); 

    const email = data.email;
    const wasFound = await db.User.findOne({ login: true }, {
        where: {
            email: {
                [Op.eq]: email
            },
        }
    })

    if (wasFound != null) {
        next();
    } else {
        console.log(chalk.red('No ha iniciado sesion'));
        res.status(401).send('No ha iniciado sesion');
    }
}

// --------------------------------------------------------------------------------------------------------------------------------------------------- \\ 

// VALIDO QUE SEA ADMINISTRADOR 
const valAdmin = async (req, res, next) => {
    const token = req.headers.authorization.replace('Bearer ', '');

    const data = jwt.decode(token, key);

    const email = data.email;
    const wasFound = await db.User.findOne({
        where: {
            email: {
                [Op.eq]: email
            },  
            admin: {
                [Op.eq]: true
            }
        }
    })
    
    if (wasFound != null) {
        next();
    } else {
        console.log(chalk.red('Necesita ser administrador'));
        res.status(403).send('Necesita ser administrador');
    }
}


module.exports = {
    valFieldSgnUp,
    valEmail,
    valSuspension,
    valFieldLogin,
    valLogin,
    valLogged,
    valAdmin
}