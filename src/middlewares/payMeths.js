const chalk = require('chalk');
const e = require('express');
const { Op } = require('sequelize');
const db = require('../database/sync');

// VALIDAR QUE NO HAYA UN METODO DE PAGO IGUAL A OTRO 
const valMeth = (req, res, next) => {
    const { title } = req.body;
    let flag = false;
    payMeth.find((payMeth) => {
        if (payMeth.title == title) {
            flag = true
            console.log(chalk.red('Ya existe un metodo de pago con este nombre'));
            res.send('Ya existe un metodo de pago con este nombre');
        }
    });
    if (flag == false) {
        next();
    }
}; 

module.exports = {
    valMeth
}