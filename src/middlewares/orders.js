const chalk = require('chalk');
const e = require('express');
const { Op } = require('sequelize');
const db = require('../database/sync');


// VALIDO QUE EL ESTADO DE LA ORDEN NO SEA DIFERENTE A LOS EXISTENTES
const valStateOrder = (req, res, next) => {
    if (req.body.state < 1 || req.body.state > 5) {
        console.log(chalk.red('El estado ingresado no es valido. Vuelva a intentarlo'));
        res.status(404).send('El estado ingresado no es valido. Vuelva a intentarlo');
    } else {
        next();
    }
}; 

// VALIDO QUE EL DETALLE DE PEDIDO EXISTA 
const valDetailOrder = async (req, res, next) => {
    const { detailId }  = req.params.id; 

    const found = await db.Detail.findOne({
        where: {
            id: {
                [Op.eq]: detailId
            }
        }
    }) 

    if (found != null) {
        next();
    } else {
        console.log(chalk.red('El id del detalle de pedido que ingreso no existe'));
        res.status(404).send('El id del detalle de pedido que ingreso no existe');
    }
}

// CONTROLO QUE EL METODO DE PAGO SEA CORRECTO
const payControl = (id) => {
    
};

// CONTROLO QUE EL PEDIDO EXISTA 
const controlOrder = (order_) => {

} 

module.exports = { 
    valStateOrder,
    valDetailOrder,
    payControl,
    controlOrder
}