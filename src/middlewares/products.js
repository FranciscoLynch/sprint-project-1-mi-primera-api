const chalk = require('chalk');
const e = require('express');
const { Op } = require('sequelize');
const db = require('../database/sync');

// VALIDAR QUE NO HAYA UN PRODUCTO IGUAL A OTRO
const valProduct = async (req, res, next) => {

    const { name } = req.body; 
    
    try {
        const products = await db.Product.findAll({
            where: {
                name: {
                    [Op.eq]: name
                }
            }
        });
        if (products != null) {
            console.log(chalk.red('Ya existe un producto con este nombre'));
            res.status(406).send('Ya existe un producto con este nombre');
        } else {
            next();
        }
    } catch (error) {
        console.log(chalk.red('Ha ocurrido un error con el producto ', error));
    }
};


module.exports = {
    valProduct
}