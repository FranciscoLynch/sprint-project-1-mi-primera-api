const chalk = require('chalk');
const redis = require('redis');
const { redis_host, redis_port, redis_url } = require('../config');

// CREA LA CONEXION CON EL SERVIDOR DE REDIS
const client = redis.createClient({
    url: redis_url,
});

// MUESTRA UN MENSAJE SI OCURRE UN ERROR EN LA CONEXION
client.on('error', (error) => {
    console.error(chalk.red('Ha ocurrido un error en la conexion con Redis', error));
})


// VERIFICA QUE INICIE CORRECTAMENTE 
client.on('connect', () => {
    console.log(chalk.green(`Conectado a Redis en el puerto ${process.env.REDIS_PORT}`));
})

module.exports = {
    client
}