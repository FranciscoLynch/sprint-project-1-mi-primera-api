// TRAIGO LOS MODELOS DE LAS TABLAS 
const { Adress } = require('../models/Adress');
const { Detail } = require('../models/DetailOrder');
const { Order } = require('../models/Order');
const { PayMeth } = require('../models/PayMeth');
const { Product } = require('../models/Product');
const { StateOrder } = require('../models/StateOrder');
const { User } = require('../models/User');

// USUARIOS

// UN USUARIO TIENE UNA O MUCHAS DIRECCIONES
User.hasMany(Adress, {
    onDelete: 'cascade'
});
Adress.belongsTo(User);
// UN USUARIO TIENE UNO O MUCHOS PEDIDOS
User.hasMany(Order, {
    onDelete: 'cascade'
});
Order.belongsTo(User);

// UN USUARIO TIENE UNO O MUCHOS DETALLES DE PEDIDOS
User.hasMany(Detail, {
    onDelete: 'cascade'
});
Detail.belongsTo(User);

// PEDIDOS

// UN PEDIDO TIENE UNA DIRECCION 
Adress.hasOne(Order);
Order.belongsTo(Adress);
// UN PEDIDO TIENE UN ESTADO
StateOrder.hasMany(Order);
Order.belongsTo(StateOrder);
// UN PEDIDO TIENE UN PAGO
PayMeth.hasMany(Order);
Order.belongsTo(PayMeth);
// UN PEDIDO TIENE UN DETALLE
/* Order.hasMany(Detail, {
    onDelete: 'cascade'
});
Detail.belongsTo(Order); */

// UN DETALLE DEL PEDIDO TIENE UNO O VARIOS PRODUCTOS 
Product.hasMany(Detail, {
    onDelete: 'cascade', 
    onUpdate: 'cascade'
});
Detail.belongsTo(Product);
