const chalk = require('chalk');
const { Sequelize } = require('sequelize');
const { database, user, password, host, dialect, port } = require('../config');
const mysql = require('mysql2/promise');

const sequelize = new Sequelize(/* database, user, password,*/ {
    host: host,
    dialect: dialect,
    port: port
});

// SE VALIDA LA CONEXION A LA BASE DE DATOS
(async function () {
    try {
        await sequelize.authenticate();
        console.log(chalk.green('La conexion ha sido establecida exitosamente'));
    } catch (err) {
        console.error(chalk.red('Incapaz de conectarse a la base de datos >>>  ', err));
    }
})();

// SE EXPORTA LA BASE DE DATOS
module.exports = {
    sequelize
}