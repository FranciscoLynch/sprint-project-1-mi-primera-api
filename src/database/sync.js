const { Sequelize, Op } = require('sequelize');
const { sequelize } = require('./conn');
const chalk = require('chalk');

// SE LLAMA A LAS TABLAS
const { Order } = require('../models/Order'),
    { PayMeth } = require('../models/PayMeth'),
    { Product } = require('../models/Product'),
    { StateOrder } = require('../models/StateOrder'),
    { User } = require('../models/User'),
    { Adress } = require('../models/Adress'),
    { Detail } = require('../models/DetailOrder');

const { associations } = require('./assoc');
// SE SINCRONIZAN LAS TABLAS
sequelize.sync({ force: false }).then(() => {
    console.log(chalk.green('Las tablas se sincronizaron'));
});

// CREO LOS METODOS DE PAGO, PRODUCTOS, ESTADOS DE PEDIDO Y USUARIO ADMINISTRADOR
/*
(async () => {
    try {

        const stateOrders = await StateOrder.findOne({
            where: {
                state: {
                    [Op.or]: ['Nuevo', 'Confirmado', 'Preparando', 'Enviando', 'Entregado']
                }
            }
        });

        if (stateOrders != null) {

            console.log(chalk.red('Los estados para los pedidos ya han sido agregados'));

        } else {

            await StateOrder.create({
                state: 'Nuevo'
            });
            await StateOrder.create({
                state: 'Confirmado'
            });
            await StateOrder.create({
                state: 'Preparando'
            });
            await StateOrder.create({
                state: 'Enviando'
            });
            await StateOrder.create({
                state: 'Entregado'
            });
        }

        const admin = await User.findOne({
            where: {
                user: {
                    [Op.eq]: 'admin'
                }
            }
        });

        if (admin != null) {

            console.log(chalk.red('El usuario administrador ya ha sido agregado'));

        } else {

            await User.create({
                user: 'admin',
                password: 'Mimamamemima123*',
                name: '-',
                lastname: '-',
                email: 'admin@gmail.com',
                phone: 1,
                adress: '-',
                twitterId: 0,
                admin: true,
                login: false,
                active: true
            });
        }

        const payMeths = await PayMeth.findOne({
            where: {
                name: {
                    [Op.or]: ['Efectivo', 'Tarjeta de credito', 'Tarjeta de debito', 'Bitcoin']
                }
            }
        });

        if (payMeths != null) {

            console.log(chalk.red('Los metodos de pago ya han sido agregados'));

        } else {

            await PayMeth.create({
                name: 'Efectivo'
            });
            await PayMeth.create({
                name: 'Tarjeta de credito'
            });
            await PayMeth.create({
                name: 'Tarjeta de debito'
            });
            await PayMeth.create({
                name: 'Bitcoin'
            });

        }

        const products = await Product.findOne({
            where: {
                name: {
                    [Op.or]: ['Bagel de salmón', 'Hamburguesa clásica', 'Sandwich veggie', 'Ensalada veggie', 'Focaccia', 'Sandwich Focaccia']
                }
            }
        });

        if (products != null) {

            console.log(chalk.red('Los productos ya han sido agregados'));

        } else {

            await Product.create({
                name: 'Bagel de salmón',
                price: 425,
            });

            await Product.create({
                name: 'Hamburguesa clásica',
                price: 350,
            });

            await Product.create({
                name: 'Sandwich veggie',
                price: 310,
            });

            await Product.create({
                name: 'Ensalada veggie',
                price: 340,
            });

            await Product.create({
                name: 'Focaccia',
                price: 300,
            });

            await Product.create({
                name: 'Sandwich Focaccia',
                price: 440,
            });

            console.log(chalk.green('El conjunto de productos se ha agregado con exito'));

        }

    } catch (error) {
        console.log(chalk.red('Ha ocurrido un error al intentar agregar informacion predeterminada ', error));
    }

 })();
 */

 // SE EXPORTAN LAS TABLAS
module.exports = {
    Order,
    PayMeth,
    Product,
    StateOrder,
    User,
    Adress,
    Detail
};

