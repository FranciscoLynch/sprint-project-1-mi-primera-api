const router = require('express').Router();
const control = require('../../controllers/controlProducts');
const midwU = require('../../middlewares/users');
const midwP = require('../../middlewares/products');
// PRODUCTOS

// VER TODOS LOS PRODUCTOS 
router.get('/list', midwU.valLogged, control.list);

// CREAR UN NUEVO PRODUCTO
router.post('/new', midwU.valLogged, midwU.valAdmin, midwP.valProduct, control.newP);

// EDITAR UN PRODUCTO
router.put('/edit', midwU.valLogged, midwU.valAdmin, midwP.valProduct, control.edit);

// ELIMINAR UN PRODUCTO
router.delete('/destroy/:id', midwU.valLogged, midwU.valAdmin, control.delet);

// EXPORTO LAS RUTAS
module.exports = router;