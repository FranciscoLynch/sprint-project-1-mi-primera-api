const router = require('express').Router();
const control = require('../../controllers/controlOrders');
const midwU = require('../../middlewares/users');
const midwO = require('../../middlewares/orders');

// UN PEDIDO PUEDE ESTAR FORMADO POR MUCHOS DETALLES DE PEDIDO, COMO UN CARRITO

// HACER UN DETALLE DE PEDIDO
router.post('/add', midwU.valLogged, control.createDetail);

// EDITAR UN DETALLE DE PEDIDO
router.put('/edit/:id', midwU.valLogged, control.editDetail);

// ELIMINAR UN DETALLE DE PEDIDO
router.delete('/delet/:id', midwU.valLogged, control.deletDetail);

// CREAR EL PEDIDO
router.post('/create', midwU.valLogged, control.createOrder);

// CONFIRMAR EL PEDIDO( SE ACUMULAN TODOS LOS DETALLES DE PEDIDO QUE NO HAYAN SIDO CONFIRMADOS HASTAS EL MOMENTO)
router.put('/confirm', midwU.valLogged, control.confirm);

// VER MI HISTORIAL DE PEDIDOS
router.get('/myOrders', midwU.valLogged, control.myOrders);

// VER TODOS LOS PEDIDOS
router.get('/list', midwU.valLogged, midwU.valAdmin, control.list);

// CAMBIAR EL ESTADO DE LOS PEDIDOS
router.put('/list/states/:id', midwU.valLogged, midwU.valAdmin, control.listEdit);

// EXPORTO LAS RUTAS
module.exports = router;