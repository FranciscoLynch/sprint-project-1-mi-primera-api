const router = require('express').Router();
const control = require('../../controllers/controlUsers');
const midw = require('../../middlewares/users');

// REGISTRO
router.post('/signup', midw.valFieldSgnUp, midw.valEmail, control.signup);

// LOGIN - INICIAR SESION 
// router.put('/login', midw.valFieldLogin, midw.valSuspension, midw.valLogin, control.login);

// LOGOUT - CERRAR SESION 
router.put('/logout', midw.valFieldLogin, control.logout);

// LISTA DE USUARIOS 
router.get('/list', midw.valLogged, midw.valAdmin, control.list);

// SUSPENDER UN USUARIO
router.put('/suspend/:id', midw.valLogged, midw.valAdmin, control.suspend);

// ACTIVAR UN USUARIO 
router.put('/active/:id', midw.valLogged, midw.valAdmin, control.activate);

// AGREGAR UNA DIRECCION
router.post('/add/adress', midw.valLogged, control.addAdress);

// EXPORTO LAS RUTAS
module.exports = router;