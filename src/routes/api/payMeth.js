const router = require('express').Router();
const control = require('../../controllers/controlPayMeth');
const midwP = require('../../middlewares/payMeths');
const midwU = require('../../middlewares/users');

// METODOS DE PAGO 

// VER TODOS LOS METODOS DE PAGO
router.get('/list', midwU.valLogged, control.list);

// CREAR UN NUEVO METODO DE PAGO
router.post('/new', midwU.valLogged, midwU.valAdmin, control.newPM);

// EDITAR UN METODO DE PAGO
router.put('/edit', midwU.valLogged, midwU.valAdmin, control.edit);

// ELIMINAR UN METODO DE PAGO
router.delete('/delete/:id', midwU.valLogged, midwU.valAdmin, control.delet);

// EXPORTO LAS RUTAS
module.exports = router;