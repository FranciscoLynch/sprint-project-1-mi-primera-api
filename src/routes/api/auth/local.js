const express = require('express');
const passport = require('passport');

function prepareRouter() {
  const router = express.Router();

  router.post('/local/auth', passport.authenticate('local', {
    failureRedirect: '/failed',
    session: false,
  }), (req, res) => {
    res.json({message: `El usuario ${req.user.user} inicio sesion con exito`});
  });

  return router;
}

module.exports = prepareRouter;