const express = require('express');
const passport = require('passport');

function prepareRouter() {
    const strategy_name = 'twitter';
    const router = express.Router();

    router.get('/twitter/auth', passport.authenticate(strategy_name, {
        failureRedirect: '/'
    }));

    router.get('/twitter/auth/callback', passport.authenticate(strategy_name, {
        failureRedirect: '/login'
    }),
        function (req, res) {
            // Successful authentication, redirect home.
            console.log('TODO SALIO BIEN :D');
            res.send('SE HA LOGUEADO CON TWITTER :D');
        });

    router.get('/login', (req, res) => {
        res.redirect('/')
    })

    return router;
} 

module.exports = prepareRouter;
