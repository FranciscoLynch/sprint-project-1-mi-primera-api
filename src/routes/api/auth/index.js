const express = require('express');
const router = express.Router();
const google = require('./google');
const facebook = require('./facebook');
const linkedin = require('./linkedin');
const local = require('./local');
const twitter = require('./twitter');

function prepareRoutes() {
  router.get('/failed', (req, res) => res.send('You Failed to log in!'))
  router.use(google());
  router.use(facebook());
  router.use(linkedin());
  router.use(local());
  router.use(twitter());
  return router

}

module.exports = prepareRoutes; 