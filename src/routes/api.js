const router = require('express').Router(); 

const apiOrders = require('./api/orders');
const apiPayMeth = require('./api/payMeth');
const apiProducts = require('./api/products');
const apiUsers = require('./api/users'); 

router.use('/orders', apiOrders); 
router.use('/payMeths', apiPayMeth); 
router.use('/products', apiProducts); 
router.use('/users', apiUsers); 

module.exports = router;