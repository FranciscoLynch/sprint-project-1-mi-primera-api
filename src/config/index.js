require('dotenv').config();

// SERVER PORT 
const server_port = process.env.PORT;
const server_host = process.env.HOST;

// DATABASE CONFIG
const database = process.env.MYSQL_DATABASE;
const user = process.env.MYSQL_USER;
const password = process.env.MYSQL_PASSWORD;
const host = process.env.MYSQL_HOST;
const dialect = process.env.MYSQL_DIALECT;
const port = process.env.MYSQL_PORT;

// JSON WEB TOKEN 
const key = process.env.KEY;
const time = process.env.TIME;

// REDIS 
const redis_host = process.env.REDIS_HOST;
const redis_port = process.env.REDIS_PORT;
const redis_url = process.env.REDIS_URL;

// CRYPTO 
const secret = process.env.SECRET;

// SWAGGER 
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

const swaggerOptions = {
    swaggerDefinition: {
        info: {
            description: "",
            title: 'Mi APP Live',
            version: '2.0.0',
            contact: {
                email: "panchengue@gmail.com"
            },
            license: {
                name: "Nginx",
                url: "https://nginx.org/LICENSE"
            },
            termsOfService: "http://swagger.io/terms/",
        },
        externalDocs: {
            description: "Find out more about our store",
            url: "http://swagger.io"
        },
        host: "www.plynch.tk",
        schemes: ["https", "http"],
    },
    apis: ['./src/swagger-doc/swagger.yaml'],
}


const swaggerDocs = swaggerJsDoc(swaggerOptions);


// EXPORTO 
module.exports = {
    database,
    user,
    password,
    host,
    dialect,
    port,
    server_port,
    server_host,
    key,
    time,
    redis_host,
    redis_port,
    redis_url,
    swaggerDocs,
    swaggerUI,
    secret
}