const chalk = require('chalk');
const db = require('../database/sync');
const serv = require('../services/products');
const { Op } = require('sequelize');
const { client } = require('../database/cache');
// USAR VARIABLES - VAR 

// VER TODOS LOS PRODUCTOS
async function list(req, res) {

    // VERIFICO SI LOS PRODUCTOS EXISTEN EN LA CACHE
    client.get('products ', async (err, data) => {
        if (err) {
            res.status(400).send(err);
        }
        // SI EXISTEN, SE ENVIAN
        if (data) {
            console.log(chalk.green('Se ha enviado la lista de producto ', { data: data }));
            res.status(202).json({ data: data });
            // SINO, SE BUSCAN EN LA BASE DE DATOS Y SE GUARDAN EN LA CACHE POR UNA HORA
        } else {
            console.log(chalk.red('No existe la llave, se creara una nueva'));

            const list = await serv.List();

            client.setex('products', 60 * 60, JSON.stringify(list), (error) => {
                if (error) {
                    console.log(chalk.red('No se ha podido enviar la lista de productos ', error));
                    res.status(400).send('No se ha podido enviar la lista de productos');
                }
                console.log(chalk.green('Se ha enviado la lista de producto ', list));
                res.status(202).json({ list });
            });
        }
    });

}

// CREAR UN PRODUCTO 
async function newP(req, res) {

    const { name, price } = req.body;

    await serv.Create(name, price).then(product => {
        console.log(chalk.green('El producto se ha creado con exito'));
        res.status(202).json({ product });
    }).catch((error) => {
        console.log(chalk.red('Ha ocurrido un error ', error));
        res.status(404).send('Ha ocurrido un error al intentar crear el producto');
    });
}

// EDITAR UN PRODUCTO
async function edit(req, res) {

    const { id, name, price } = req.body;

    const product = await serv.Edit(id, name, price).catch(error => {
        console.log(chalk.red('Ha ocurrido un error ', error));
        res.status(404).send('El producto no se ha podido editar');
    }) 
    console.log(chalk.yellow(product))

    if (product === false) {
        client.exists('products', (err, data) => {
            if (data === 1) {
                client.del('products', (err, deleted) => {
                    if (deleted === 1) {
                        console.log(chalk.green('La cache se limpio exitosamente'));
                    }
                })
            }
        });
    }

    console.log(chalk.green('El producto se ha editado con exito '));
    res.status(202).send('El producto se ha editado con exito ');
}

// ELIMINAR UN PRODUCTO
async function delet(req, res) {

    await serv.Delet(req).then(() => {
        console.log(chalk.green('El producto se ha eliminado con exito'));
        res.status(202).send('El producto se ha eliminado con exito');
    }).catch(error => {
        console.log(chalk.red('Ha ocurrido un error al intentar eliminar el producto ', error));
        res.status(404).send('Ha ocurrido un error y no se elimino el producto');
    })
}

// EXPORTO LOS PRODUCTOS
module.exports = {
    newP,
    edit,
    delet,
    list,
    
}