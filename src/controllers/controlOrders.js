const chalk = require('chalk');
const db = require('../database/sync');
const serv = require('../services/orders');
const jwt = require('jsonwebtoken');
const { key } = require('../config');

// CREAR UN DETALLE DE PEDIDO 
async function createDetail(req, res) {
    const { product_id, amount } = req.body;

    const token = req.headers.authorization.replace('Bearer ', '');

    const email = jwt.decode(token, key);

    await serv.CreateDetail(email, product_id, amount).then(() => {
        console.log(chalk.green('El producto se agrego al carrito'));
        res.status(202).send('El producto se agrego al carrito');
    }).catch(error => {
        console.log(chalk.red('No se ha podido agregar el producto ', error));
        res.status(404).send('No se ha podido agregar el producto');
    });

}

// EDITAR EL DETALLE DE PEDIDO 
async function editDetail(req, res) {
    const detailId = req.params.id;
    const { amount } = req.body; 

    await serv.EditDetail(detailId, amount).then(() => {
        console.log(chalk.green('Su modificacion se realizo correctamente'));
        res.status(202).send('Su modificacion se realizo correctamente');
    }).catch(error => {
        console.log(chalk.red('No se ha podido modificar el producto ', error));
        res.status(404).send('No se ha podido modificar el producto');
    }); 

}

// ELIMINAR EL DETALLE DE PEDIDO 
async function deletDetail(req, res) {
    const detailId = req.params.id; 

    await serv.DeletDetail(detailId).then(() => {
        console.log(chalk.green('Se elimino correctamente'));
        res.status(202).send('Se elimino correctamente');
    }).catch(error => {
        console.log(chalk.red('No se ha podido eliminar el producto ', error));
        res.status(404).send('No se ha podido eliminar el producto');
    }); 
}

// CREAR EL PEDIDO
async function createOrder(req, res) {
    const token = req.headers.authorization.replace('Bearer ', ''); 
    const email = jwt.decode(token, key);

    const { adressID, payMethID} = req.body; 

    await serv.CreateOrder(email, adressID, payMethID).then(() => {
        console.log(chalk.green('Su pedido se ha creado'));
        res.status(202).send('Su pedido se ha creado');
    }).catch(error => {
        console.log(chalk.red('No se ha podido crear su pedido')); 
        res.status(404).send('No se ha podido crear su pedido');
    })
}

// CONFIRMAR EL PEDIDO 
async function confirm(req, res) {
    const token = req.headers.authorization.replace('Bearer ', ''); 
    const email = jwt.decode(token, key); 
    
    await serv.Confirm(email).then(() => {
        console.log(chalk.green('Su pedido se ha confirmado'));
        res.status(202).send('Su pedido se ha confirmado');
    }).catch(error => {
        console.log(chalk.red('No se ha podido confirmar su pedido')); 
        res.status(404).send('No se ha podido confirmar su pedido');
    })
}

// VER EL HISTORIAL DE MIS PEDIDOS
async function myOrders(req, res) {
    
    const token = req.headers.authorization.replace('Bearer ', '');

    const email = jwt.decode(token, key);
    
    await serv.History(email).then((history) => {
        console.log(chalk.green('Se ha enviado su historial de pedidos')); 
        res.status(202).json({history});
    }).catch(error => {
        console.log(chalk.red('Ha ocurrido un error al intentar enviar su historial de pedidos ', error));
        res.status(404).send('Ha ocurrido un error al intentar enviar su historial de pedidos');
    })
}

// VER TODOS LOS PEDIDOS 
async function list(req, res) {
    await serv.ListOrders().then((list) => {
        console.log(chalk.green('Se ha enviado la lista de pedidos')); 
        res.status(202).json( [{list}]);
    }).catch(error => {
        console.log(chalk.red('Ha ocurrido un error al intentar enviar la lista de pedidos ', error));
        res.status(404).send('Ha ocurrido un error al intentar enviar la lista de pedidos');
    })
}

// CAMBIAR EL ESTADO DE LOS PEDIDOS
async function listEdit(req, res) {
    const orderId = req.params.id;
    const { state } = req.body;
    console.log(orderId, state)
    serv.ChangeStates(orderId, state).then(() => {
        console.log(chalk.green(`Se ha cambiado el estado del pedido ${orderId}`));
        res.status(202).send(`Se ha cambiado el estado del pedido ${orderId}`);
    }).catch(error => {
        console.log(chalk.red('No se ha podido cambiar el estado del pedido ', error)); 
        res.status(404).send('No se ha podido cambiar el estado del pedido');
    })

}



module.exports = {
    createDetail,
    editDetail,
    deletDetail,
    createOrder,
    confirm,
    myOrders,
    list,
    listEdit
}


/* 
    HACER UN PEDIDO  

    const { order_, payMeth_, adress } = req.body; // en payMeth_ se ingresa el id del metodo de pago
    let idUser = 0;
    const find = users.find((user) => {
        if (user.login == true) {
            idUser = user.id;
        }
    });
    let d = new Date();
    let hour = `${d.getHours()}:${d.getMinutes()}`;
    let i = users.findIndex(x => x.id == idUser);
    let desc = String();
    let total = new Float32Array();

    order_.forEach(order => {
        desc += `X${order.amount}:${order.product.name}`;
        total += parseFloat(order.amount * order.product.price);
    });

    if (payControl(payMeth_)) {
        const doOrder = {
            id: orders[orders.length - 1].id + 1,
            state: stateOrder[0].id,
            time: hour,
            description: desc,
            total: total,
            payMeth: payMeth_,
            idUser: idUser,
            nameUser: users[i].user,
            adress: adress,
        };

        orders.push(doOrder);
        console.log(chalk.green('Pedido añadido'));
        res.send('Pedido añadido');
    } else {
        console.log(chalk.red('Metodo de pago incorrecto'));
        res.send('Metodo de pago incorrecto');
    }

    -------------------------------------------------------------------------------------------------------------

    EDITAR UN PEDIDO  

     const { order_, payMeth_, newAdress } = req.body; // en payMeth_ se ingresa el id del metodo de pago

    const found = users.findIndex(user => user.login == true);

    let adress;
    if (newAdress == "") {
        adress = false;
    } else {
        adress = newAdress;
    }

    const contOrder = controlOrder(order_); 

    -------------------------------------------------------------------------------------------------------------

    CONTROLO QUE EL PEDIDO EXISTA

    /* 
    let description = "";
    let import_ = 0;
    let flag = 0;

    order_.forEach(item => {
        if (item.amount <= 0) {
            return false;
        } else {
            /* products seria el array del primer sprint, tengo que cambiarlo para que se conecte con las tablas con sequelize */
            /* products.forEach(product => {
                if (product.id == item.id) {
                    descripcion += `${product.short} x ${item.amount} `;
                    import_ += (product.price * item.amount);
                    flag++;
                }
            });
        }
    });

    if (flag != order_.length) {
        return false;
    } else {
        return (compraDetalle = {
            description: description.trim(),
            import_
        })
    } 
    
    -------------------------------------------------------------------------------------------------------------
    
    CONTROLO QUE EL METODO DE PAGO SEA CORRECTO 
    
    if (payMeth.find(x => x.id === id)) {
        return true;
    } else {
        return false;
    }
    
    -------------------------------------------------------------------------------------------------------------
    
    CAMBIAR EL ESTADO DE LOS PEDIDOS 

    const { id, idState } = req.body; // en state se ingresa el id del estado
    let flag = false;
    orders.map((order) => {
        if (order.id == id) {
            order.state.id = idState;
            flag = true;
            console.log(chalk.green(`Se actualizo el estado de la orden ${id}`));
            res.send(`Se actualizo el estado de la orden ${id}`);
        }
    });
    if (flag == false) {
        console.log(chalk.red('No se encontro el numero de la orden ingresada'));
        res.send('No se encontro el numero de la orden ingresada');
    }
    
    -------------------------------------------------------------------------------------------------------------
    CONFIRMAR EL PEDIDO 

    const idOrder = parseInt(req.params.id);
    const idUser = users.find(user => user.login === true);

    let i = orders.findIndex(order => order.id === idOrder);

    if (index != -1) {
        if (orders[index].id_user != idUser) {
            console.log(chalk.red('No se puede modificar el pedido'));
            res.status(400).send('No se puede modificar el pedido');
        } else {
            orders[index].state = 2;
            console.log(chalk.green('Su pedido fue confirmado'));
            res.send('Su pedido fue confirmado');
        }
    }
    
    ------------------------------------------------------------------------------------------------------------- 

    VER MI HISTORIAL DE PEDIDOS 
    
     let userId = 0;
    const found = users.map((user) => {
        if (user.login === true) {
            userId = user.id
        }
    });
    const myHistory = orders.filter(x => x.idUser === userId);
    myHistory.length > 0 ? res.json(myHistory) : res.send('No tiene pedidos realizados');


*/
