const chalk = require('chalk');
const serv = require('../services/payMeth');

// METODOS DE PAGO 

// VER TODOS LOS METODOS DE PAGO 
async function list(req, res) {

    await serv.List().then(list => {
        console.log(chalk.green('Se ha enviado la lista de los metodos de pago '));
        res.status(202).json({ list });
    }).catch(error => {
        console.log(chalk.red('Ha ocurrido un error al intentar enviar la lista de los metodos de pago'));
        res.status(400).send('No se ha podido enviar la lista de los metodos de pago');
    });

}

// CREAR UN NUEVO METODO DE PAGO 
async function newPM(req, res) {

    await serv.Create(req).then(() => {
        console.log(chalk.green('Se ha añadido el metodo de pago '));
        res.status(202).json('Se ha añadido el metodo de pago');
    }).catch(error => {
        console.log(chalk.red('Ha ocurrido un error al intentar crear el metodo de pago ', error));
        res.status(400).send('No se ha podido crear el metodo de pago');
    });

}

// EDITAR UN METODO DE PAGO 
async function edit(req, res) {

    const { id, name } = req.body;

    await serv.Edit(id, name).then(() => {
        console.log(chalk.green('El metodo de pago se ha editado con exito'));
        res.status(202).send('El metodo de pago se ha editado con exito');
    }).catch(error => {
        console.log(chalk.red('Ha ocurrido un error al intentar editar el metodo de pago ', error));
        res.status(400).send('No se ha podido editar el metodo de pago');
    })

}

// ELIMINAR UN METODO DE PAGO
async function delet(req, res) {

    await serv.Delet(req).then(() => {
        console.log(chalk.green('El metodo de pago se ha eliminado con exito'));
        res.status(202).send('El metodo de pago se ha eliminado con exito');
    }).catch(error => {
        console.log(chalk.red('No se encontro el metodo de pago y no se elimino'));
        res.status(400).send('No se encontro el metodo de pago y no se elimino');
    }); 

}

module.exports = {
    newPM,
    edit,
    list,
    delet
}
