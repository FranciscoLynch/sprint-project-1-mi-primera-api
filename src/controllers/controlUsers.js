const chalk = require('chalk');
const jwt = require('jsonwebtoken');
const { key, secret } = require('../config');
const serv = require('../services/users');
const { createHmac } = require('crypto');

// REGISTRO DE USUARIO 
async function signup(req, res) {

    const { user, pw, name, lastname, email, phone, adress } = req.body;

    // SE ENCRYPTA LA CONTRASEÑA  
    const password = createHmac('sha256', secret)
        .update(pw)
        .digest('hex');

    console.log(chalk.cyan('HASH >> ', password));

    // SE CREA EL USUARIO EN LA BASE DE DATOS
    await serv.Create(user, password, name, lastname, email, phone, adress).then((user) => {
        console.log(chalk.green('Se ha creado el usuario con exito'));
        res.status(202).send('Se ha creado el usuario con exito');
    }).catch((error) => {
        res.status(404).send('No se ha podido registrar al usuario - linea 18', error);
    })
}

// INICIO DE SESION/LOGIN
async function login(email, password) {

    await serv.Login(email, password).then(() => {

        const token = jwt.sign(email, key);

        console.log(chalk.green('TOKEN >> ', token));
        console.log(chalk.green('El usuario se ha logueado'));

        res.status(202).json({ token });

    }).catch((error) => {
        console.log(chalk.red('Ha ocurrido un error al intentar iniciar sesion ', error));
        res.status(404).send('Ha ocurrido un error al intentar iniciar sesion');
    })

}

// CERRAR SESION
async function logout(req, res) {
    const { email, password } = req.body;

    await serv.Logout(email, password).then(() => {
        console.log(chalk.green('El usuario ha cerrado sesion'));
        res.status(202).send('El usuario ha cerrado sesion');
    }).catch(error => {
        console.log(chalk.red('Ha ocurrido un error al intentar cerrar sesion ', error));
        res.status(404).send('Ha ocurrido un error al intentar cerrar sesion');
    });
}

// VER LA LISTA DE USUARIOS (ADMINISTRADOR)
async function list(req, res) {

    await serv.List().then((users) => {
        console.log(chalk.green('Se ha enviado la lista de usuarios '));
        res.status(202).json({ users })
    }).catch(error => {
        console.log(chalk.red('Ha ocurrido un error al enviar la lista de usuarios'));
        res.status(404).send('Ha ocurrido un error al enviar la lista de usuarios');
    });
}

// SUSPENSION DE USUARIO (ADMINISTRADOR)
async function suspend(req, res) {
    const id = req.params.id;

    await serv.Suspend(id).then(() => {
        console.log(chalk.green(`El usuario con el id ${id} ha sido suspendido`));
        res.status(202).send(`El usuario con el id ${id} ha sido suspendido`);
    }).catch(error => {
        console.log(chalk.red('No se ha podido suspender al usuario'));
        res.status(404).send('No se ha podido suspender al usuario');
    });
}

// ACTIVACION DE USUARIO (ADMINISTRADOR)
async function activate(req, res) {
    const id = req.params.id;

    await serv.Activate(id).then(() => {
        console.log(chalk.green(`Al usuario con el id ${id} se le ha revocado la suspension`));
        res.status(202).send(`Al usuario con el id ${id} se le ha revocado la suspension`);
    }).catch(error => {
        console.log(chalk.red('No se ha podido revocar la suspension al usuario'));
        res.status(404).send('No se ha podido revocar la suspension al usuario');
    });
}

async function addAdress(req, res) {
    const token = req.headers.authorization.replace('Bearer ', '');
    const email = jwt.decode(token, key);
    const { newAdress } = req.body;

    await serv.createTheAdress(email, newAdress).then(() => {
        console.log(chalk.green(`La direccion se agrego con exito`));
        res.status(202).send(`La direccion se agrego con exito`);
    }).catch(error => {
        console.log(chalk.red('No se pudo agregar la direccion'));
        res.status(404).send('No se pudo agregar la direccion');
    });

}

module.exports = {
    signup,
    // login,
    logout,
    list,
    suspend,
    activate,
    addAdress,
}