const { Sequelize, DataTypes, Model } = require("sequelize");
const { sequelize } = require('../database/conn');

class Order extends Model { };

Order.init(
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        time: DataTypes.TIME,
        total: DataTypes.INTEGER,
    },
    {
        sequelize,
        modelName: 'orders',
        timestamps: false
    }
)

module.exports = { Order };

// CUANDO HAGA UN PEDIDO VALIDAR QUE LA DIRECCION INGRESADA EXISTA Y COINCIDA CON EL ID DEL USUARIO 

