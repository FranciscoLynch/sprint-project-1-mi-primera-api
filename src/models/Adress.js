const { Sequelize, DataTypes, Model } = require("sequelize");
const { sequelize } = require('../database/conn');

class Adress extends Model { };

Adress.init(
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        adress: DataTypes.STRING,
    },
    {
        sequelize,
        modelName: 'adresses',
        timestamps: false
    }
);

module.exports = { Adress };