const { DataTypes, Model } = require("sequelize");
const { sequelize } = require('../database/conn');

class PayMeth extends Model { };

PayMeth.init(
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        name: {
            type: DataTypes.STRING,
            notNull: true,
        }
    },
    {
        sequelize,
        modelName: 'payMeths',
        timestamps: false
    }
)

module.exports = { PayMeth };

/*
// METODOS DE PAGO
const payMeth = [
    {
        id: 1,
        title: 'Cash'
    },
    {
        id: 2,
        title: 'Debit'
    },
    {
        id: 3,
        title: 'Credit'
    },
    {
        id: 4,
        title: 'Transfer'
    }
]; */