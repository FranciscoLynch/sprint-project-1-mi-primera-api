const { DataTypes, Model } = require("sequelize");
const { sequelize } = require('../database/conn');

class StateOrder extends Model { };

StateOrder.init(
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        state: DataTypes.STRING,
    },
    {
        sequelize,
        modelName: 'statesOrder',
        timestamps: false
    }
)

module.exports = { StateOrder };

/* // ESTADOS DE LOS PEDIDOS
const stateOrder = [
    {
        id: 1,
        title: 'Pendiente'
    },
    {
        id: 2,
        title: 'Confirmado'
    },
    {
        id: 3,
        title: 'En preparacion'
    },
    {
        id: 4,
        title: 'Enviado'
    },
    {
        id: 5,
        title: 'Entregado'
    }
]; */