const { Sequelize, DataTypes, Model } = require("sequelize");
const { sequelize } = require('../database/conn');

class Detail extends Model { };

Detail.init(
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        amount: {
            type: DataTypes.INTEGER,
            notNull: true,
        },
        paid: {
            type: DataTypes.BOOLEAN,
            notNull: true,
        }
    },
    {
        sequelize,
        modelName: 'detailsOrder',
        timestamps: false
    }
)

module.exports = { Detail };