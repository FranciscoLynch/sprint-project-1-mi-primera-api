const { Sequelize, DataTypes, Model } = require("sequelize");
const { sequelize } = require('../database/conn');

class Product extends Model { };

Product.init(
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        name: {
            type: DataTypes.STRING,
            notNull: true,
        },
        price: {
            type: DataTypes.INTEGER,
            notNull: true,
        }
    },
    {
        sequelize,
        modelName: 'products',
        timestamps: false
    }
)

module.exports = { Product };
