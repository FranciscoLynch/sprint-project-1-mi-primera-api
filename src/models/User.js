const { Sequelize, DataTypes, Model } = require("sequelize");
const { sequelize } = require('../database/conn');

class User extends Model { };

User.init(
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        user: {
            type: DataTypes.STRING,
            notNull: false,
        },
        password: {
            type: DataTypes.STRING,
            notEmpty: false,
            notNull: false,
        },
        name: {
            type: DataTypes.STRING,
            notNull: false,
        },
        lastname: {
            type: DataTypes.STRING,
            notNull: false,
        },
        email: {
            type: DataTypes.STRING,
            isEmail: false,
            notNull: false,
        },
        phone: {
            type: DataTypes.INTEGER,
            isNumeric: false,
            notNull: false,
        },
        adress: {
            type: DataTypes.STRING, 
            notNull: false,
        },
        twitterId: {
            type: DataTypes.STRING,
            notNull: false,
        },
        admin: DataTypes.BOOLEAN,
        login: DataTypes.BOOLEAN,
        active: DataTypes.BOOLEAN
    },
    {
        sequelize,
        modelName: 'users',
        timestamps: false
    }
)

module.exports = { User };