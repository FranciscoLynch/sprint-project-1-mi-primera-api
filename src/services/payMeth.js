const users = require('../controllers/controlProducts');
const { Op } = require('sequelize');
const db = require('../database/sync');
const chalk = require('chalk');

const List = async function () {

    try {
        const payMeths = await db.PayMeth.findAll();
        return payMeths;
    } catch (error) {
        console.log(chalk.red('Ha ocurrido un error con la lista de los metodos de pago - LINEA  12', error));
    }
}

const Create = async function (req) {
    try {
        await db.PayMeth.create(req.body);
    } catch (error) {
        console.log(chalk.red('Ha ocurrido un error - LINEA 20 ', error));
    }
}

const Edit = async function (id, name) {
    try {
        await db.PayMeth.update({ name: name }, {
            where: {
                id: {
                    [Op.eq]: id
                }
            }
        });
    } catch (error) {
        console.log(chalk.red('Ha ocurrido un error - LINEA 34', error));
    }
}

const Delet = async function (req) {
    try {
        await db.PayMeth.destroy({
            where: {
                id: {
                    [Op.eq]: req.params.id
                }
            }
        })
    } catch (error) {
        console.log(chalk.red('Ha ocurrido un error - LINEA 48 ', error));
    }
}

module.exports = {
    List,
    Create,
    Edit,
    Delet
}