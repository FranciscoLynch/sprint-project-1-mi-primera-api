const prepareGoogleStrategy = require('./auth/passport-google');
const prepareFacebookStrategy = require('./auth/passport-facebook');
const prepareLocalStrategy = require('./auth/passport-local');
const prepareTwitterStrategy = require('./auth/passport-twitter');
// require('./facebook');

function initialize() {
    prepareGoogleStrategy()
    prepareFacebookStrategy()
    prepareLocalStrategy()
    prepareTwitterStrategy()
}

module.exports = initialize;