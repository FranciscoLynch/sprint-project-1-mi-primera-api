const users = require('../controllers/controlUsers');
const { Op } = require('sequelize');
const db = require('../database/sync');
const { secret } = require('../config');
const chalk = require('chalk');
const { createHmac } = require('crypto');

const Create = async function (user, password, name, lastname, email, phone, adress) {

    await db.User.create({
        user: user,
        password: password,
        name: name,
        lastname: lastname,
        email: email,
        phone: phone,
        adress: adress,
        admin: false,
        login: false,
        active: true
    }).then(async (user) => {
        await db.Adress.create({
            adress: adress,
            userId: user.id
        });
    }).catch(error => {
        console.error(chalk.red('Ha ocurrido un error - LINEA 22', error));
    });
}

const Login = async function (email, password) {

    try {
        const user = await db.User.findOne({ login: false }, {
            where: {
                email: {
                    [Op.eq]: email
                },
                password: {
                    [Op.eq]: password
                },
            }
        })
        await user.update({ login: true });

        // SE ENCRYPTA LA CONTRASEÑA
        const pw = createHmac('sha256', secret)
            .update(password)
            .digest('hex');

        console.log(chalk.cyan('HASH >> ', pw));
        return user
    } catch (error) {
        console.log(chalk.red('Ha ocurrido un error - LINEA 78', error));
    }
}

const Logout = async function (email, password) {

    await db.User.findOne({ login: true }, {
        where: {
            email: {
                [Op.eq]: email
            },
            password: {
                [Op.eq]: password
            },
        }
    }).then(async (user) => {
        await user.update({ login: false });
    }).catch((error) => {
        console.log(chalk.red('Ha ocurrido un error - LINEA 78', error));
    });
}

const List = async function () {

    const users = await db.User.findAll().catch(error => {
        console.log(chalk.red('Ha ocurrido un error con la lista de usuarios - LINEA 120 ', error));
    });
    return users;

}

const Suspend = async function (id) {

    await db.User.update({ active: false }, {
        where: {
            id: {
                [Op.eq]: id
            }
        }
    }).catch((error) => {
        console.log(chalk.red('Ha ocurrido un error - LINEA 134 ', error));
    });

}

const Activate = async function (id) {

    await db.User.update({ active: true }, {
        where: {
            id: {
                [Op.eq]: id
            }
        }
    }).catch((error) => {
        console.log(chalk.red('Ha ocurrido un error - LINEA 134 ', error));
    });

}

const createTheAdress = async function (email, newAdress) {

    try {
        await db.User.findOne({
            where: {
                email: {
                    [Op.eq]: email
                }
            }
        }).then(async (user) => {
            await db.Adress.create({
                adress: newAdress,
                userId: user.id
            })
        })
    } catch (error) {
        console.log(chalk.red('Ha ocurrido un error - LINEA 127', error))
    }
}

module.exports = {
    Create,
    Login,
    List,
    Suspend,
    Logout,
    Activate,
    createTheAdress
}