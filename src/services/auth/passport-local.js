const passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy;
const serv = require('../../services/users');

async function prepareStrategy() {
  passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    session: false
  },
    (email, password, done) => {
      serv.Login(email, password)
        .then(user => {
          if (user === null) {
            done(null, false);
          } else {
            done(null, user);
          }
        })
        .catch(error => {
          done(error);
        })
    }
  ));
}

module.exports = prepareStrategy;