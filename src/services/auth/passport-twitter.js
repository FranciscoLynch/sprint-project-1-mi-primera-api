const passport = require('passport');
const passportTwitter = require('passport-twitter');
const db = require('../../database/sync');


function createTwitterStrategy() {
    const TwitterStrategy = passportTwitter.Strategy;

    const strategy_name = 'twitter';

    passport.serializeUser(function (user, cb) {
        cb(null, user);
    });

    passport.deserializeUser(function (obj, cb) {
        cb(null, obj);
    });

    passport.use(strategy_name, new TwitterStrategy({
        consumerKey: process.env.TWITTER_CONSUMER_KEY,
        consumerSecret: process.env.TWITTER_CONSUMER_SECRET,
        callbackURL: process.env.TWITTER_CALLBACK
    },
        async function (token, tokenSecret, profile, cb) {
            console.log('>>> PROFILE <<< ', profile)
            try {
                const user = await db.User.findOne({
                    where: {
                        twitterId: profile.id
                    }
                });
                console.log('>>>> ESTO ES EL USUARIO <<<<', user);
                (async function (err, user) {
                    if (user != null) {
                        console.log('RETURNEANDO USUARIO');
                        return cb(err, user);
                    } else {
                        await db.User.create({
                            user: profile.name,
                            password: null,
                            name: profile.screen_name,
                            lastname: null,
                            email: null,
                            phone: null,
                            adress: null,
                            twitterId: profile.id,
                            admin: false,
                            login: false,
                            active: true
                        }).then((user) => {
                            return cb(err, user);
                        })
                    }
                })();
            } catch (error) {
                console.log('Ocurrio un error en la estrategia de Twitter', error);
            }
        }));

}


module.exports = createTwitterStrategy;