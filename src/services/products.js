const users = require('../controllers/controlProducts');
const { Op } = require('sequelize');
const db = require('../database/sync');
const chalk = require('chalk');

const List = async function () {

    try {
        const products = await db.Product.findAll();
        return products;
    } catch (error) {
        console.log(chalk.red('Ha ocurrido un error con la lista de productos - LINEA  ', error));
    }
}

const Create = async function (name, price) {
    try {
        const product = await db.Product.create({
            name: name,
            price: price,
        });
        return product;
    } catch (error) {
        console.log(chalk.red('Ha ocurrido un error - LINEA 24 ', error));
    }
}

const Edit = async function (id, name, price) {
    try {
        let price1 = await db.Product.findOne({
            where: {
                id: {
                    [Op.eq]: id
                }
            }
        });
        console.log(price1);
        await db.Product.update({ name: name, price: price }, {
            where: {
                id: {
                    [Op.eq]: id
                }
            }
        }); 
        let price2 = await db.Product.findOne({
            where: {
                id: {
                    [Op.eq]: id
                }
            }
        }); 
        console.log(price2)
        if (price1 === price2){
            return true; 
        } else { 
            return false;
        }

    } catch (error) {
        console.log(chalk.red('Ha ocurrido un error - LINEA 38 ', error));
    }
}

const Delet = async function (req) {
    try {
        await db.Product.destroy({
            where: {
                id: {
                    [Op.eq]: req.params.id
                }
            }
        })
    } catch (error) {
        console.log(chalk.red('Ha ocurrido un error - LINEA 52 ', error));
    }
}

module.exports = {
    List,
    Create,
    Edit,
    Delet
}