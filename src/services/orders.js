const orders = require('../controllers/controlOrders');
const { Op } = require('sequelize');
const db = require('../database/sync');
const chalk = require('chalk');

const CreateDetail = async function (email, product_id, amount) {

    await db.User.findOne({
        where: {
            email: {
                [Op.eq]: email
            }
        }
    }).then(async (user) => {
        await db.Detail.create({
            amount: amount,
            paid: false,
            userId: user.id,
            productId: product_id,
        })
    }).catch(error => {
        console.log(chalk.red('Ha ocurrido un error LINEA 21 ', error));
    });
}

const EditDetail = async function (detailId, amount) {

    await db.Detail.findOne({
        where: {
            id: {
                [Op.eq]: detailId
            }
        }
    }).then(async (detail) => {
        await detail.update({ amount: amount });
    }).catch(error => {
        console.log(chalk.red('Ha ocurrido un error - LINEA 36', error));
    });
}

const DeletDetail = async function (detailId) {

    await db.Detail.destroy({
        where: {
            id: {
                [Op.eq]: detailId
            }
        }
    }).catch(error => {
        console.log(chalk.red('Ha ocurrido un error - LINEA 36', error));
    });
}



const CreateOrder = async function (email, adressID, payMethID) {

    try {
        // DECLARO VARIABLES GLOBALES QUE SE VAN A IMPLEMENTAR AL CREAR EL PEDIDO
        var total = 0;
        var user_id;
        var d = new Date();
        var time = `${d.getHours()}:${d.getMinutes()}`;
        var amount_;

        // BUSCO AL USUARIO PARA SABER SU ID
        await db.User.findOne({
            where: {
                email: {
                    [Op.eq]: email
                }
            }
        }).then(async (user) => {
            user_id = user.id
            console.log(chalk.cyan('User id', user_id));
            //BUSCO TODOS LOS DETALLES DE PEDIDO QUE TENGAN SU ID Y QUE NO ESTEN PAGOS
            await db.Detail.findAll({
                where: {
                    userId: {
                        [Op.eq]: user.id
                    },
                    paid: {
                        [Op.eq]: false
                    }
                }
            }).then(async (details) => {
                console.log('DETAILS ', details);
                // ITERO EL ARRAY DE DETALLES DE PEDIDO PARA EXTRAER LA INFORMACION DE LA CANTIDAD, PRECIO Y NOMBRE DE CADA PRODUCTO
                for (let i = 0; i < details.length; i++) {
                    // OBTENGO LA INFORMACION DE LA CANTIDAD
                    amount_ = details[i].amount;

                    let price_;
                    let pId = details[i].productId
                    // CON EL ID DEL PRODUCTO LO BUSCO EN LA TABLA
                    await db.Product.findOne({
                        where: {
                            id: {
                                [Op.eq]: pId
                            }
                        }
                    }).then(product => {
                        // OBTENGO SU PRECIO 
                        price_ = product.price;
                    })
                    // LOS ACUMULO 
                    total = total + (amount_ * price_);

                }
            }).then(async () => {
                // CREO EL PEDIDO APLICANDO LA INFORMACION OBTENIDA ANTERIORMENTE
                await db.Order.create({
                    time: time,
                    total: total,
                    userId: user_id,
                    adressId: adressID,
                    statesOrderId: 1,   // AL CREARSE EL PEDIDO SE QUEDA EN ESTADO PENDIENTE HASTA QUE SE CONFIRME 
                    payMethId: payMethID
                })
            })
        })

    } catch (error) {
        console.log(chalk.red('Ha ocurrido un error LINEA 124 ', error));
    }

}

const Confirm = async function (email) {


    try {
        const user = await db.User.findOne({
            where: {
                email: {
                    [Op.eq]: email
                }
            }
        }).then(async (user) => {
            await db.Order.update({ statesOrderId: 2 }, {
                where: {
                    userId: {
                        [Op.eq]: user.id
                    },
                    statesOrderId: {
                        [Op.eq]: 1
                    }
                }
            })
        })
    } catch (error) {
        console.log(chalk.red('Ha ocurrido un error LINEA 153'));
    }
}

const History = async function (email) {

    console.log(email)
    try {
        await db.User.findOne({
            where: {
                email: {
                    [Op.eq]: email
                }
            }
        }).then(async (user) => {
            await db.Order.findAll({
                where: {
                    userId: {
                        [Op.eq]: user.id
                    }
                }
            })
        })
    } catch (error) {
        console.log(chalk.red('Ha ocurrido un error - LINEA 171 ', error));
    }
}

const ListOrders = async function () {

    await db.Order.findAll().then(async (list) => {
        console.log('LIST', list)
    }).catch(error => {
        console.log(chalk.red('Ha ocurrido un error - LINEA 180 ', error));
    });
}

const ChangeStates = async function (orderId, state) {

    try {
        await db.Order.findOne({
            where: {
                id: {
                    [Op.eq]: orderId
                }
            }
        }).then(async (order) => {
            console.log('ORDER ', order)
            await order.update({
                statesOrderId: state
            })
        })
    } catch (error) {
        console.log(chalk.red('Ha ocurrido un error - LINEA 190 ', error));
    }
}

module.exports = {
    CreateDetail,
    CreateOrder,
    EditDetail,
    DeletDetail,
    Confirm,
    History,
    ListOrders,
    ChangeStates
}