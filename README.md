# Delilah Restó API

Delilah Restó API es una API Rest que interactúa con un sistema de base de datos SQL construida con el objetivo de garantizar una gestión persistente del restaurante ficticio **Delilah Resto**.

## Comenzando 🚀

Haz un clon del repositorio

```
git clone https://gitlab.com/FranciscoLynch/sprint-project-1-mi-primera-api.git
```

Instala las dependencias

```
npm install
```

### Instrucciones 📄

**1 - Instalacion**

* Antes de prender el servidor local, es necesario instalar un sistema de gestion de bases de datos SQL. El autor utiliza WampServer64, pero cualquier otra alternativa es valida, como por ejemplo, XAMPP. Al tener dicho sistema, para poder conectarse a la base de datos debe iniciar sesion en phpMyAdmin. Ingrese en usuario "root"(sin las comillas) y la contraseña vacia, y seleccione MySQL. Una vez que inicie sesion debe crear una base de datos, para hacerlo debe presionar donde dice Nueva y escribir en el nombre de la base de datos "sprint-project-db"(sin las comillas) y crear. Una vez realizado, tan solo debe iniciar el servidor para que se carguen los modelos de las tablas.

* Si lo desea puede instalar RedisDesktopManager para monitorear la informacion de la cache. 

**2 - Ejecucion**


Para iniciar mi proyecto, tienes que usar `npm run start` y para iniciar el test usa `npm run test`.

**3 - Inserción de datos**

* Al iniciar el servidor y sincronizarse las tablas en la base de datos, no se cargan a la primera los datos por predeterminado. Para que suceda debe reiniciar el servidor ya que primero se cargan los modelos de las tablas y luego los datos. Estos consisten en algunos productos, metodos de pago, estados para los pedidos y un administrador.

* Los datos del usuario con el rol administrador son **email: "-", password "Mimamamemima123*"**

**4 - Swagger**

* Para acceder a la documentacion en Swagger ingrese a https://www.plynch.tk/api-docs
  
  ## Construido con 🛠️

* [NodeJS]("https://nodejs.org/es/")
* [Express]("https://expressjs.com/es/") 
* [Swagger]("https://swagger.io/)
* [MySQL]("https://www.mysql.com/")
* [Redis]("https://redis.io/")
* [Mocha]("https://mochajs.org/")
* [JWT]("https://jwt.io/")
* [Postman]("https://www.postman.com/")

## Autor ✒️

* **Francisco Lynch** - [FranciscoLynch](https://gitlab.com/FranciscoLynch) 

