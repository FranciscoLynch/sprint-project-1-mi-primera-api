const dotenv = require('dotenv');
dotenv.config();
// CONFIGURO EXPRESS 
const express = require('express');
const session = require('express-session');
const app = express();
const chalk = require('chalk');
const { server_port, swaggerDocs, swaggerUI } = require('./src/config');
const cors = require('cors')
const passport = require('passport');
const initServices = require('./src/services/index');
const initPublicRouter = require('./src/routes/public');
const prepareRoutes = require('./src/routes/api/auth/index');
const { auth } = require('express-openid-connect');
const initMercadoPagoRouter = require('./src/routes/api/mercadopago');
const initPaypalPayment = require('./src/routes/api/paypal');

function initializeAuth0(app) {
    const config = {
        authRequired: false,
        auth0Logout: true,
        baseURL: process.env.AUTH0_LOCAL_URL,
        clientID: process.env.AUTH0_CLIENT_ID,
        issuerBaseURL: process.env.AUTH0_BASE_URL,
        secret: process.env.AUTH0_SECRET,
        // routes: {}
    };
    console.log({
        ...config,
        secret: `1`
    })
    app.use(auth(config));
}

function main() {
    app.use(cors());
    initServices(app);

    // Initializes passport and passport sessions
    app.use(passport.initialize());
    initializeAuth0(app);
    // CONFIGURACION
    app.use(express.json());
    // app.use(express.bodyParser());
    app.use(express.urlencoded({ extended: true }));

    app.use(session({
        secret: process.env.SESSION_SECRET,
    }));
    app.use(session({
        secret: process.env.SESSION_SECRET,
        resave: true,
        saveUninitialized: true,
        cookie: { secure: true }
    })) 

    app.use(initPublicRouter());
    app.use(initMercadoPagoRouter());
    app.use(initPaypalPayment());
    app.use(prepareRoutes());

    app.use(express.static('./public'));

    app.set('port', process.env.PORT || 3000);

    // SWAGGER 
    app.use('/api-docs',
        swaggerUI.serve,
        swaggerUI.setup(swaggerDocs));

    // LLAMO A LA BASE DE DATOS CONFIGURADA PARA CONSTRUIR LAS TABLAS 
    require('./src/database/conn');
    require('./src/database/sync');
    require('./src/database/assoc');
    require('./src/database/cache');

    // herramientas de modelado 

    // IMPORTO LAS RUTAS QUE CONVERGEN EN API.JS 
    const apiRouter = require('./src/routes/api');
    app.use('/api', apiRouter);

    // AGREGO SEGURIDAD AL SERVIDOR USANDO HELMET
    const helmet = require('helmet');
    app.use(helmet());

    /* app.use((err, req, res, next) => {
        if(err) {
            console.log(err);
            return res.status(500).send('Ocurrio un error');
        } 
        next();
    }) */
    // CREA UN SERVIDOR LOCAL 
    app.listen(app.get('port'), (req, res) => {
        console.log(chalk.green(`SERVIDOR CORRIENDO EN PUERTO ${app.get('port')}`));
    });
}

main(); 