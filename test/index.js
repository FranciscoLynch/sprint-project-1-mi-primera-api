const chai = require('chai');
const assert = require('chai').assert;
const fetch = require('node-fetch');
const chaiHttp = require('chai-http');

chai.use(chaiHttp);
const url = 'http://localhost:3000'

describe(' Rutina de evaluacion al registro de usuarios', () => {

    it('El usuario se registra correctamente', (done) => {
        const x = Math.round(Math.random() * 100);
        chai.request(url).post('/api/users/signup').send({
            user: "raaaul",
            password: "123",
            name: "Raul",
            lastname: "Gomez",
            email: `raul${x}@gmail.com`,
            phone: 12345
        }).end((err, res) => {
            if (err) {
                throw err
            }
            // console.log(res);
            chai.expect(res.status).equals(202);
            done();
        }).timeout(10000);


    });
    /* 
        it('Error si el email ya fue utilizado', async () => {
            
    
        });
    
        it('Error si hay algun campo incompleto o vacio', async () => {
    
        }); */
})
